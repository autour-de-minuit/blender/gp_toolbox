import bpy
import shutil
import subprocess
from pathlib import Path
import os
from os.path import basename
# import re

from .utils import show_message_box

"""## not used for now 
class GPTB_OT_check_git(bpy.types.Operator):
    '''check if git is in path'''
    bl_idname = "gptb.check_git"
    bl_label = "Check if git is in system path"
    bl_options = {'REGISTER', 'INTERNAL'}
    
    def invoke(self, context, event):
        self.ok = shutil.which('git')
        return context.window_manager.invoke_props_dialog(self, width=250)
    
    def draw(self, context):
        layout = self.layout
        if self.ok:
            layout.label(text='Ok ! git is in system PATH', icon='INFO')
        else:
            layout.label(text='Git is not in system PATH', icon='CANCEL')
            layout.operator('wm.url_open', text='Download And Install From Here', icon='URL').url = 'https://git-scm.com/download/'
"""


def git_update(folder: str) -> str:
    ''' Try to git pull fast foward only in passed folder and return console output'''
    os.chdir(folder)
    name = basename(folder)
    print(f'Pulling in {name}')
    pull_cmd = ['git', 'pull', '--ff-only'] # git pull --ff-only
    pull_ret = subprocess.check_output(pull_cmd)
    return pull_ret.decode()


class GPTB_OT_git_pull(bpy.types.Operator):
    """Update addon with git pull if possible"""
    bl_idname = "gptb.git_pull"
    bl_label = "Gptoolbox Git Pull Update"
    bl_options = {'REGISTER', 'INTERNAL'}
    
    # def invoke(self, context, event):
    #     return self.execute(context)

    # def draw(self, context):

    def execute(self, context):
        if not shutil.which('git'):
            self.report({'ERROR'}, 'Git not found in path, if just installed, restart Blender/Computer')
            return {'CANCELLED'}
        
        ret = git_update(Path(__file__).parent.as_posix())

        if 'Already up to date' in ret:
            self.report({'INFO'}, 'Already up to date')
            show_message_box(ret.rstrip('\n').split('\n'))
        elif 'Fast-forward' in ret and 'Updating' in ret:
            self.report({'INFO'}, 'Updated ! Restart Blender')
            show_message_box(['Updated! Restart Blender.'] + ret.rstrip('\n').split('\n'))
            
        return {'FINISHED'}

classes = (
# GPTB_OT_check_git,
GPTB_OT_git_pull,
)

def register():
    if bpy.app.background:
        return
    for cl in classes:
        bpy.utils.register_class(cl)

def unregister():
    if bpy.app.background:
        return
    for cl in reversed(classes):
        bpy.utils.unregister_class(cl)
