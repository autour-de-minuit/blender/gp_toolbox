## Pure keymaping additions

import bpy
addon_keymaps = []

def register_keymaps():
    addon = bpy.context.window_manager.keyconfigs.addon
    # km = addon.keymaps.new(name = "3D View", space_type = "VIEW_3D")# in 3D context
    # km = addon.keymaps.new(name = "Window", space_type = "EMPTY")# from everywhere
    

    ## Sculpt mode toggles
    km = addon.keymaps.new(name = "Grease Pencil Stroke Sculpt Mode", space_type = "EMPTY", region_type='WINDOW')

    kmi = km.keymap_items.new('wm.context_toggle', type='ONE', value='PRESS')
    kmi.properties.data_path='scene.tool_settings.use_gpencil_select_mask_point'
    addon_keymaps.append((km, kmi))

    kmi = km.keymap_items.new('wm.context_toggle', type='TWO', value='PRESS')
    kmi.properties.data_path='scene.tool_settings.use_gpencil_select_mask_stroke'
    addon_keymaps.append((km, kmi))

    kmi = km.keymap_items.new('wm.context_toggle', type='THREE', value='PRESS')
    kmi.properties.data_path='scene.tool_settings.use_gpencil_select_mask_segment'
    addon_keymaps.append((km, kmi))

    ## T temp cutter (need disabling of native T shortcut, maybe expose a button to set the shortcut as user ?)
    # km = addon.keymaps.new(name = "Grease Pencil", space_type = "EMPTY")
    # kmi = km.keymap_items.new('gpencil.stroke_cutter', type='LEFTMOUSE', value='PRESS', key_modifier='T')
    # kmi.properties.flat_caps=False
    # addon_keymaps.append((km, kmi))

def unregister_keymaps():
    for km, kmi in addon_keymaps:
        km.keymap_items.remove(kmi)
    
    addon_keymaps.clear()



def register():
    if not bpy.app.background:
        register_keymaps()

def unregister():
    if not bpy.app.background:
        unregister_keymaps()

if __name__ == "__main__":
    register()
