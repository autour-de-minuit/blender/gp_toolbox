# GP toolbox

Blender addon - Various tool to help with grease pencil in animation productions.

## /!\ This repository is archived

The updated project is now hosted on ADM's gitea: https://git.autourdeminuit.com/autour_de_minuit/gp_toolbox


---
Everything below is related to the archived repository


**[Download latest](https://gitlab.com/autour-de-minuit/blender/gp_toolbox/-/archive/master/gp_toolbox-master.zip)**

**[Demo video](https://www.youtube.com/watch?v=Htgao_uPWNs)**

**[Readme Doc in French](README_FR.md)**

It is recommended to enable _Grease Pencil Tools_ native Blender addon. Brings essentials features:  
Canvas rotation, Box deform, Timeline scrubbing in viewport, Quick layer navigator.

---  

## Description

<!-- ![pseudo color demo](https://github.com/Pullusb/images_repo/blob/master/GPT_pseudo_tint.gif)
*In this demo F9 is pressed to call the "redo panel" and modify the hue offset with constant update*   -->
  
In sidebar (N) > Gpencil > Toolbox

<!-- - Pseudo tint color : Tints each GP layer of selected GP objects with a pseudo random tints to quickly identify which lines belong to which layer.  
/!\ Using this will override the tint and tint factor on your layer (so they will be lost if you used it for a specific layer) -->

## Addon preferences

important point of addon preferences:  

Set path to the palette folder (there is a json palette IO but you an also put a blend and use a blend importer)

Note about palette : For now the importer is not working with linked palette as it's not easy for animator (there are properties of the material you cannot access and the link grey-out fade the real color in UIlist preview)


### Environnement Variables

> Mainly for devellopers to set project environnement

Since 1.5.2, following _environnement variable_ can set the project properties in toolbox preferences at register launch:

`RENDER_WIDTH` : resolution x  
`RENDER_HEIGHT` : resolution y  
`FPS` : project frame rate  
`PALETTES` : path to the blends (or json) containing materials palettes  
`BRUSHES` : path to the blend containing brushes to load  
`PREFIXES` : list of prefix (comma separated uppercase letters (2), an optional tooltip can be set after `:`, ex: 'LN:Line, CO:color, SH:Shadow')  <!-- between 1 and 6 character -->
`SUFFIXES` : list of suffixes (comma separated uppercase letters of 2 character, ex: 'OL,UL')  
`SEPARATOR` : Separator character to determine prefixes, default is '_' (should not be a special regex character)  

### Expose native functionnality

The panel expose some attributes that are too "far" in the UI:

- Zoom 1:1 - Camera view take 100% zoom according to current scene resolution (ops `view3d.zoom_camera_1_to_1`)
- Zoom fit - Adjust view so camera frame takes full viewport spac (ops `view3d.view_center_camera`)
<!-- - autolock layer - tick layers'autolock  -->
- In Front - the `In Front` property of the object to get an X-ray view
- passepartout camera - enable/disable + set opacity
- button and sliders to enable / disable / set opacity of single background camera images

**Edit line opacity** - Animators usually like to hide completely edit lines to have a better view of the drawing in edit/sculpt mode, lowering opacity also allows a better reading on what's selected.  
This options is stored per layer per object but this apply on everything at once.

### Passive action

An "on save" Handler that trigger relative remap of all path can be enabled in addon prefs (disabled by default).

### function

- Mirror flip : If in cam view flip the camera X scale value (you can see and draw mnirrored to see problems)

<!-- - Overlay toggle : (toggle pref and overlay)  -->

- quick access to scene camera passepartout toggle and opacity

- quick access to scene camera background images visibility with individual references toggle.

- Basic playblast and viewport playblast:

  - dedicated resolution percentage value

  - can auto launch and/or auto open folder at finished (option in addon preferences)

- Jump to GP keyframe :

  - Choose key to Auto bind in addon prefs (since 0.9.3).  
  > Manual setup: Add two keymap shortcut in _windows_ or _screen(global)_ with indentifier `screen.gp_keyframe_jump`, one should have `next` toggled off to jump back

- GP paint cutter tool temporary switch shortcut
  - Map manually to a key with `wm.temp_cutter` (This one needs "Any" as press mode) or `wm.sticky_cutter` (Modal sticky-key version)

- Snap cursor to GP canvas operator accessible with `view3d.cusor_snap` 
  - Map nanually (might be autoreplaced according to version) by replacing entry using `view3d.cursor3d` in 3D View category (defaut shortcut `Shift + Right-clic`)

- Follow cursor toggle : When activated the cursor follow the active the active object

- [breakdowner operator for object mode](https://blenderartists.org/t/pose-mode-animation-tools-for-object-mode/1221322), auto-keymap on : Shift + E 

- Line extender help closing gaps between lines with control over layer target (-> need also control over frame targets)

- GP copy paste : world space cut-copy-paste (bake strokes). `Ctrl + Shift + X/C/V`
Store strokes in os'clipboard (easier cross blend copy)
cutting is use a more user friendly  (leave boundary points of left strokes untouched).  
Also Possible to copy whole selected layers.

<!-- - Auto update : you have an updater in the addon preference tabs (use the [CGcookie addon updater](https://github.com/CGCookie/blender-addon-updater)) -->


**Palette management**

In material submenu you have mutliple new entry:

- Copy Materials To Selected : copy all material to other selected GP (difference with Ctrl+L > materials is that it doesn't erase materials, only append those that are not in other's materials stack)

- Load/Save Json palette : Save/load materials externally to a json file from/to the active material stack.

- Load Color palette : same as the load above exept it loads directly from a blend file (all the material that the blend contains)

- Clean materials

**Shortcuts**

Viewport:

  - Layer Picker from closest stroke in paint mode using quick press on `W` for stroke (and `alt+W` for fills)

  - Material Picker (`S` and `Alt+S`) quick trigger, change is only triggered if key is pressed less than 200ms

  - `F2` in Paint and Edit to rename active layer

  - `Insert` add a new layer (same as Krita)

  - `Shift + Insert` add a new layer and immediately pop-up a rename box

  - `page up / page down` change active layer up/down with a temporary fade (settings in addon prefs)

  - `Shift + E` breakdown animation in object Mode

  - `Ctrl + Shift + X/C/V` - Worldspace cut/copy/paste selected strokes/points:

Dopesheet:

  - `Ctrl + Shift + X` Cut and send to layer
  
  - `Ctrl + Shift + D` Duplicate and send to layer

Sculpt:

  - point/stroke filter shortcut on `1`, `2`, `3` as toggles (similar to edit mode native shortcuts)


### Where ?

Panel in sidebar : 3D view > sidebar 'N' > Gpencil

<!--
## Todo:

- Allow to render resolution from cam name

- Update multi output system:
    - use render layers + file outputs instead of batch by tweaking visibility

- BG Playblast enhancement:
    - Add a prefs to fallback to "simple" render.

- Settings-io for projects : Rsolution du film, palette folder, render settings...

- expose "tool setting" of canvas handling in sidebar (visible only in draw mode)

- Move automatically view to match GP Front (depending on Gpencil view settings)

- move GP keyframes selection and Object keyframe selection simultaneouly (Already Done by Tom Viguier at [Andarta](https://gitlab.com/andarta-pictures)  -->


---

Consult [Changelog here](CHANGELOG.md)