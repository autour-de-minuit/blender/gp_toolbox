## Can be renamed and used as standalone __init__.py file
# bl_info = {
# "name": "GP guided colorize",
# "description": "Grease pencil colorisation helpers",
# "author": "Samuel Bernou",
# "version": (0, 1, 0),
# "blender": (2, 82, 0),
# "location": "3D view > Gpencil > Colorize",
# "warning": "WIP",
# "doc_url": "",
# "category": "3D View",
# }

import bpy

# from . import OP_line_closer
from . import OP_create_empty_frames


## Colorize properties
class GPCOLOR_PG_settings(bpy.types.PropertyGroup) :    
    # extend_layers: bpy.props.BoolProperty(name='Extend layers' default=True, description="Work on selected layers, else only active")
    extend_layer_tgt : bpy.props.EnumProperty(
        name="Extend layers", description="Choose which layer to target",
        default='ACTIVE',
        items=(
            ('ACTIVE', 'Active only', 'Target active layer only', 0),#include icon name in fourth position
            ('SELECTED', 'Selected', 'Target selected layers in GP dopesheet', 1),
            ('ALL_VISIBLE', 'All visible', 'target all visible layers', 2),
            ('ALL', 'All', 'All (even locked and hided layer)', 2),
            ))

    extend_selected: bpy.props.BoolProperty(name='Extend selected', default=False, description="Work on selected stroke only if True, else All stroke")
    extend_length: bpy.props.FloatProperty(name='Extend length', default=0.01, precision=3, step=0.01, description="Length for extending strokes boundary")

    deviation_tolerance : bpy.props.FloatProperty(
    name="Deviation angle", description="Deviation angle tolerance of last point(s) to be considered accidental trace",
    default=1.22, min=0.017, max=3.124, soft_min=0.017, soft_max=1.92, step=3, precision=2, unit='ROTATION')#, subtype='ANGLE')

classes = (
GPCOLOR_PG_settings,
)

def register():
    for cls in classes:
        bpy.utils.register_class(cls)
    OP_create_empty_frames.register()
    # OP_line_closer.register()
    bpy.types.Scene.gpcolor_props = bpy.props.PointerProperty(type = GPCOLOR_PG_settings)

def unregister():
    # OP_line_closer.unregister()
    OP_create_empty_frames.unregister()
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
    del bpy.types.Scene.gpcolor_props


if __name__ == "__main__":
    register()