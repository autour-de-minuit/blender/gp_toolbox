# from .func_gmic import *
from ..utils import (location_to_region,
                    region_to_location,
                    vector_length_2d,
                    vector_length,
                    draw_gp_stroke, 
                    extrapolate_points_by_length,
                    simple_draw_gp_stroke)

import bpy
from math import degrees
from mathutils import Vector
# from os.path import join, basename, exists, dirname, abspath, splitext

# iterate over selected layer and all/selected frame and close gaps between line extermities with a tolerance level

def get_closeline_mat(ob):
    # ob = C.object
    gp = ob.data

    # get material
    closeline = bpy.data.materials.get('closeline')
    if not closeline:
        print('Creating line closing material in material database')
        closeline = bpy.data.materials.new('closeline')
        bpy.data.materials.create_gpencil_data(closeline)#make it GP
        closeline.grease_pencil.color = [0.012318, 0.211757, 0.607766, 1.000000]#blue - [0.778229, 0.759283, 0.000000, 1.000000]# yellow urgh

    if not closeline.name in gp.materials:
        gp.materials.append(closeline)

    # get index in list
    index = None
    for i, ms in enumerate(ob.material_slots):
        if ms.material == closeline:
            index = i
            break

    if not index:
        print(f'could not find material {closeline.name} in material list')
    
    return index

def create_gap_stroke(f, ob, tol=10, mat_id=None):
    '''Take a frame, original object, an optional tolerance value and material ID
    Get all extremity points

    for each one, analyse if he is close to another in screen space
        - if it's the case mark this point as used by the other in the list (avoid to redo the other way)
        dic encounter[point:[list of points already encountered]]
    '''
    from collections import defaultdict
    encounter = defaultdict(list)
    plist = []
    matrix = ob.matrix_world
    for s in f.strokes:#add first and last
        smat = ob.material_slots[s.material_index].material
        if not smat:continue#no material on line
        if smat.grease_pencil.show_fill:continue# skip fill lines -> #smat.grease_pencil.show_stroke
        if len(s.points) < 2:continue#avoid 0 or 1 points
        plist.append(s.points[0])
        plist.append(s.points[-1])
        # plist.extend([s.points[0], s.points[-1])# is extend faster ?

    # pnum = len(plist)
    ctl = 0
    passed = []
    for i, p in enumerate(plist):
        # print(f'{i+1}/{pnum}')
        for op in plist:#other points
            if p == op:# print('same point')
                continue
            gap2d = vector_length_2d(location_to_region(matrix @ p.co), location_to_region(matrix @ op.co))
            # print('gap2d: ', gap2d)
            if gap2d > tol:
                continue
            if gap2d < 1:#less than one pixel no need
                continue
            
            # print('create_boundary_stroke')

            ## dont evaluate a point twice (skip if > 1 intersection)
            passed.append(op)
            if p in passed:
                # print('op in passed')
                continue

            ## Filter to avoid same stroke to be recreated switched
            pairlist = encounter.get(op)
            if pairlist:
                # print('is in dic')#Dbg
                if p in pairlist:
                    # print('found it')#Dbg
                    #already encountered, skip
                    continue

            # from pprint import pprint#Dbg
            # pprint(encounter)#Dbg

            # print('new line', p, op)
            # not met before, mark as encountered and create line.
            encounter[p].append(op)
            

            simple_draw_gp_stroke([p.co, op.co], f, width = 2, mat_id = mat_id)
            ctl += 1

    print(f'{ctl} line created')

##test_call: #create_gap_stroke(C.object.data.layers.active.active_frame, C.object, mat_id=C.object.active_material_index)

def create_closing_line(tolerance=0.2):
    for ob in bpy.context.selected_objects:
        if ob.type != 'GPENCIL':
            continue

        mat_id = get_closeline_mat(ob)# get a the closing material

        if not mat_id:
            print(f'object {ob.name} has no line closing material and could not create one !')
            continue
        # can do something to delete all line already there using this material
        gp = ob.data
        gpl = gp.layers
        if not gpl:continue#print(f'obj {ob.name} has no layers')

        for l in gpl:
            ## filter on selected
            if not l.select:continue# comment this line for all
            # for f in l.frames:#not all for now
            f = l.active_frame
            ## create gap stroke
            create_gap_stroke(f, ob, tol=tolerance, mat_id=mat_id)

def is_deviating_by(s, deviation=0.75):
    '''get a stroke and a deviation angle (radians, 0.75~=42 degrees)
    return true if end points angle pass the threshold'''
    
    if len(s.points) < 3:
        return

    pa = s.points[-1]
    pb = s.points[-2]
    pc = s.points[-3]
    
    a = location_to_region(pa.co)
    b = location_to_region(pb.co)
    c = location_to_region(pc.co)

    #cb-> compare angle with ba->
    angle = (b-c).angle(a-b)

    print('angle: ', degrees(angle))
    pa.select = angle > deviation
    return angle > deviation

def extend_stroke_tips(s,f,ob,length, mat_id):
    '''extend line boundary by given length'''
    for id_pair in [ [1,0], [-2,-1] ]:# start and end pair
        ## 2D mode
        # a = location_to_region(ob.matrix_world @ s.points[id_pair[0]].co)
        # b_loc = ob.matrix_world @ s.points[id_pair[1]].co
        # b = location_to_region(b_loc)
        # c = extrapolate_points_by_length(a,b,length)#print(vector_length_2d(b,c))
        # c_loc = region_to_location(c, b_loc)
        # simple_draw_gp_stroke([ob.matrix_world.inverted() @ b_loc, ob.matrix_world.inverted() @ c_loc], f, width=2, mat_id=mat_id)
        
        ## 3D
        a = s.points[id_pair[0]].co# ob.matrix_world @ 
        b = s.points[id_pair[1]].co# ob.matrix_world @ 
        c = extrapolate_points_by_length(a,b,length)#print(vector_length(b,c))
        simple_draw_gp_stroke([b,c], f, width=2, mat_id=mat_id)

def change_extension_length(ob, strokelist, length, selected=False):
    mat_id = get_closeline_mat(ob)
    if not mat_id:
        print('could not get/set closeline mat')
        return

    ct = 0
    for s in strokelist:
        if s.material_index != mat_id:#is NOT a closeline
            continue
        if len(s.points) < 2:#not enough point to evaluate
            continue
        if selected and not s.select:# select filter
            continue
        
        ## Change length of current length to designated
        # Vector point A to point B (direction), push point B in this direction
        a = s.points[-2].co
        bp = s.points[-1]#end-point
        b = bp.co
        ab = b - a
        if not ab:
            continue
        # new pos of B is A + new length in the AB direction 
        newb = a + (ab.normalized() * length)
        bp.co = newb
        ct += 1
    
    return ct

def extend_all_strokes_tips(ob, frame, length=10, selected=False):
    '''extend all strokes boundary by calling extend_stroke_tips'''
    # ob = bpy.context.object
    mat_id = get_closeline_mat(ob)
    if not mat_id:
        print('could not get/set closeline mat')
        return
    
    # TODO need custom filters or go in GP refine strokes...
    # frame = ob.data.layers.active.active_frame

    if not frame: return
    ct = 0
    #TODO need to delete previous closing lines on frame before launching
    
    # iterate in a copy of stroke list to avoid growing frame.strokes as we loop in !
    for s in list(frame.strokes):
        if s.material_index == mat_id:#is a closeline
            continue
        if len(s.points) < 2:#not enough point to evaluate
            continue
        if selected and not s.select:#filter by selection
            continue
        
        extend_stroke_tips(s, frame, ob, length, mat_id=mat_id)
        ct += 1
    
    return ct


class GPSTK_OT_extend_lines(bpy.types.Operator):
    """
    Extend lines on stroke boundarys
    """
    bl_idname = "gp.extend_close_lines"
    bl_label = "Gpencil extend closing lines"
    bl_options = {'REGISTER','UNDO'}

    @classmethod
    def poll(cls, context):
        return context.active_object is not None and context.active_object.type == 'GPENCIL'

    # mode : bpy.props.StringProperty(
    #     name="mode", description="Set mode for operator", default="render", maxlen=0, subtype='NONE', options={'ANIMATABLE'})

    layer_tgt : bpy.props.EnumProperty(
        name="Extend layers", description="Choose which layer to target",
        default='ACTIVE',
        items=(
            ('ACTIVE', 'Active only', 'Target active layer only', 0),#include icon name in fourth position
            ('SELECTED', 'Selected', 'Target selected layers in GP dopesheet (only visible)', 1),
            ('ALL_VISIBLE', 'All visible', 'target all visible layers', 2),
            ('ALL', 'All', 'All (even locked and hided layer)', 3),
            ))

    selected : bpy.props.BoolProperty(name='Selected', default=False, description="Work on selected stroke only if True, else All stroke")

    length : bpy.props.FloatProperty(name='Length', default=0.2, precision=3, step=0.01, description="length of the extended strokes")

    def invoke(self, context, event):
        # self.selected = event.shift#if shift is pressed, force inclusion of load
        ## get init value from scene prop settings
        self.selected = context.scene.gpcolor_props.extend_selected
        self.length = context.scene.gpcolor_props.extend_length
        self.layer_tgt = context.scene.gpcolor_props.extend_layer_tgt
        return self.execute(context)

    def execute(self, context):
        ob = context.object
        if self.layer_tgt == 'ACTIVE':
            lays = [ob.data.layers.active]
        elif self.layer_tgt == 'SELECTED':
            lays = [l for l in ob.data.layers if l.select and not l.hide]
        elif self.layer_tgt == 'ALL_VISIBLE':
            lays = [l for l in ob.data.layers if not l.hide]
        else:
            lays = [l for l in ob.data.layers if not any(x in l.info for x in ('spot', 'colo'))]

        fct = 0
        for l in lays:
            if not l.active_frame:
                print(f'{l.info} has no active frame')
                continue
            fct += extend_all_strokes_tips(ob, l.active_frame, length = self.length, selected = self.selected)
        
        if not fct:
            mess = "No strokes extended... see console"
            self.report({'WARNING'}, mess)#WARNING, ERROR
            return {'CANCELLED'}

        mess = f"{fct} strokes extended with closing lines"
        self.report({'INFO'}, mess)#WARNING, ERROR
        return {'FINISHED'}


class GPSTK_OT_change_closeline_length(bpy.types.Operator):
    """
    Change extended lines length
    """
    bl_idname = "gp.change_close_lines_extension"
    bl_label = "Change closeline length (use F9 to call redo panel)"
    bl_options = {'REGISTER','UNDO'}

    @classmethod
    def poll(cls, context):
        return context.active_object is not None and context.active_object.type == 'GPENCIL'

    layer_tgt : bpy.props.EnumProperty(
        name="Extend layers", description="Choose which layer to target",
        default='ACTIVE',
        items=(
            ('ACTIVE', 'Active only', 'Target active layer only', 0),#include icon name in fourth position
            ('SELECTED', 'Selected', 'Target selected layers in GP dopesheet (only visible)', 1),
            ('ALL_VISIBLE', 'All visible', 'target all visible layers', 2),
            ('ALL', 'All', 'All (even locked and hided layer)', 3),
            ))

    selected : bpy.props.BoolProperty(name='Selected', default=False, description="Work on selected stroke only if True, else All stroke")

    length : bpy.props.FloatProperty(name='Length', default=0.2, precision=3, step=0.01, description="length of the extended strokes")#step=0.00,

    def invoke(self, context, event):
        ## get init value from scene prop settings
        self.selected = context.scene.gpcolor_props.extend_selected
        self.length = context.scene.gpcolor_props.extend_length
        self.layer_tgt = context.scene.gpcolor_props.extend_layer_tgt
        return self.execute(context)

    def execute(self, context):
        ob = context.object
        if self.layer_tgt == 'ACTIVE':
            lays = [ob.data.layers.active]
        elif self.layer_tgt == 'SELECTED':
            lays = [l for l in ob.data.layers if l.select and not l.hide]
        elif self.layer_tgt == 'ALL_VISIBLE':
            lays = [l for l in ob.data.layers if not l.hide]
        else:
            lays = [l for l in ob.data.layers if not any(x in l.info for x in ('spot', 'colo'))]

        fct = 0
        for l in lays:
            if not l.active_frame:
                print(f'{l.info} has no active frame')
                continue
            fct += change_extension_length(ob, [s for s in l.active_frame.strokes], length = self.length, selected = self.selected)

        if not fct:
            mess = "No extension modified... see console"
            self.report({'WARNING'}, mess)
            return {'CANCELLED'}

        mess = f"{fct} extension tweaked"
        self.report({'INFO'}, mess)
        return {'FINISHED'}


class GPSTK_OT_comma_finder(bpy.types.Operator):
    """
    Tester to identify accidental comma trace
    """
    bl_idname = "gp.comma_finder"
    bl_label = "Strokes comma finder"
    bl_options = {'REGISTER','UNDO'}

    @classmethod
    def poll(cls, context):
        return context.active_object is not None and context.active_object.type == 'GPENCIL'
    
    def execute(self, context):
        ct = 0
        ob = context.object
        lays = [l for l in ob.data.layers if not l.hide and not l.lock]
        for l in lays:
            if not l.active_frame:continue
            for s in l.active_frame.strokes:
                if is_deviating_by(s, context.scene.gpcolor_props.deviation_tolerance):
                    ct+=1
            
        self.report({'INFO'}, f'{ct} endpoint found')#WARNING, ERROR
        return {'FINISHED'}
    

class GPSTK_PT_line_closer_panel(bpy.types.Panel):
    bl_label = "GP line stopper"# title
    ## bl_options = {'DEFAULT_CLOSED', 'HIDE_HEADER' }# closed by default, collapse the panel and the label
    ## is_popover = False # if ommited
    ## bl_space_type = ['EMPTY', 'VIEW_3D', 'IMAGE_EDITOR', 'NODE_EDITOR', 'SEQUENCE_EDITOR', 'CLIP_EDITOR', 'DOPESHEET_EDITOR', 'GRAPH_EDITOR', 'NLA_EDITOR', 'TEXT_EDITOR', 'CONSOLE', 'INFO', 'TOPBAR', 'STATUSBAR', 'OUTLINER', 'PROPERTIES', 'FILE_BROWSER', 'PREFERENCES'], default 'EMPTY'

    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Gpencil"#name of the tab
    #attach in gmic colorize ? (bad idea since gmicolor not in init and may be disable)
    # bl_parent_id = "GMICOLOR_PT_auto_color_panel"

    @classmethod
    def poll(cls, context):
        return (context.object is not None)# and context.object.type == 'GPENCIL'

    ## draw stuff inside the header (place before main label)
    # def draw_header(self, context):
    #     layout = self.layout
    #     layout.label(text="More text in header")

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        # prefs = get_addon_prefs()
        layout.prop(context.scene.gpcolor_props, 'extend_layer_tgt')
        layout.prop(context.scene.gpcolor_props, 'extend_selected')
        layout.prop(context.scene.gpcolor_props, 'extend_length')
        layout.operator("gp.extend_close_lines", icon = 'SNAP_MIDPOINT')
        
        #diplay closeline visibility
        if context.object.type == 'GPENCIL' and context.object.data.materials.get('closeline'):
            row=layout.row()
            row.prop(context.object.data.materials['closeline'].grease_pencil, 'hide', text='Stop lines')
            row.operator("gp.change_close_lines_extension", text='Length', icon = 'DRIVER_DISTANCE')
        else:
            layout.label(text='-no stop lines-')

        layout.separator()
        layout.prop(context.scene.gpcolor_props, 'deviation_tolerance')
        layout.operator("gp.comma_finder", icon = 'INDIRECT_ONLY_OFF')
        #TODO change length (on selection,  on all)
        #TODO remove all line (in unlocked frame, on all)


classes = (
GPSTK_OT_extend_lines,
GPSTK_OT_change_closeline_length,
GPSTK_OT_comma_finder,
GPSTK_PT_line_closer_panel,
)

def register():
    for cls in classes:
        bpy.utils.register_class(cls)

def unregister():
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)