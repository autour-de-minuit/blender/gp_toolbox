import bpy
from .utils import get_gp_datas, get_addon_prefs, translate_range


def get_hue_by_name(name, offset=0):
    '''
    Get a string and return a hue value
    offsetted by int [offset] value based on a range of 255
    '''

    val = []
    add = 0
    for i in name:
        add += ord(i)*8
        #val.append(str(ord(i)))
    #number = ''.join(val)
    #print("number", number)#Dbg

    # print(add, "% 255 =", add % 1000)#Dbg

    moduled = (add + offset) % 1000

    ##avoid red
    hue = translate_range(moduled, 0, 1000, 0.1, 0.9)

    ##avoid pink
    #hue = translate_range(moduled, 0, 255, 0.0, 0.7)

    return hue


class GPT_OT_auto_tint_gp_layers(bpy.types.Operator):
    bl_idname = "gp.auto_tint_gp_layers"
    bl_label = "Pseudo tint layers"
    bl_description = "Put a tint on layers according to namespace (except background)"
    bl_options = {"REGISTER", "UNDO"}

    # bpy.types.Scene.gp_autotint_offset = bpy.props.IntProperty(name="Tint hue offset", description="offset the tint by this value for better color", default=0, min=-5000, max=5000, soft_min=-999, soft_max=999, step=1)#, subtype='PERCENTAGE'
    # bpy.types.Scene.gp_autotint_namespace = bpy.props.BoolProperty(name="use prefix", description="Put same color on layers unsing the same prefix (separated by '_') of full name withjout separator", default=True)

    autotint_offset : bpy.props.IntProperty(name="Tint hue offset",
    default=0, min=-5000, max=5000, soft_min=-999, soft_max=999, step=1)#, subtype='PERCENTAGE'

    reset : bpy.props.BoolProperty(name="Reset GP tints",
    description="Put all tint factor to 0", default=False)
    
    selected_GP : bpy.props.BoolProperty(name="Selected",
    description="Work on all selected grease pencil objects, else only active one", default=True)

    def execute(self, context):
        ## TODO create a scene string variable to store serialized list of pre-tinted layers
        addon_prefs = get_addon_prefs()
        separator = addon_prefs.separator
        if not separator:separator = '_'
        # Define GP object to work on
        gp_datas = get_gp_datas(selection = self.selected_GP)

        if self.reset:
            for gp in gp_datas:
                gpl = gp.layers
                for l in gpl:
                    l.tint_factor = 0
                    # l.tint_color.hsv = (0,0,0)#Reset tint ?
                    # reset color stored if it was different than black on change
            return {"FINISHED"}

        for gp in gp_datas:
            gpl = gp.layers
            layer_ct = len(gpl)
            hue_offset = self.autotint_offset#context.scene.gptoolprops.autotint_offset
            #context.scene.gp_autotint_offset# scene property or self property

            
            # namespace_order
            namespaces=[]
            for l in gpl:
                ns= l.info.lower().split(separator, 1)[0]
                if ns not in namespaces:
                    namespaces.append(ns)

            ns_len =len(namespaces)
            namespaces.reverse()
            #print("namespaces", namespaces)#Dbg
            #print("ns_len", ns_len)#Dbg
            

            print('--------')
            ### step from 0.1 to 0.9

            for i, l in enumerate(gpl):
                if l.info.lower() not in ('background',):
                    print()
                    print('>', l.info)
                    ns= l.info.lower().split(separator, 1)[0]#get namespace from separator
                    print("namespace", ns)#Dbg

                    if context.scene.gptoolprops.autotint_namespace:
                        h = get_hue_by_name(ns, hue_offset)#l.info == individuels

                    else:
                        h = translate_range((i + hue_offset/100)%layer_ct, 0, layer_ct, 0.1, 0.9)
                    # h = hueval + hue_offset/10
                    # hueval += step
                    print("hue", h)#Dbg

                    ## Desaturate for each color per namespace index between defined range (reperesent define depth).
                    # s = translate_range(namespaces.index(ns), 0, ns_len, 0.5, 0.8)
                    s = 0.8

                    print("index", namespaces.index(ns), '/', ns_len)#Dbg
                    print("sat", s)#Dbg
                    #v = 0.8
                    v = s
                    l.tint_factor = 1
                    l.tint_color.hsv = (h,s,v)

        return {"FINISHED"}


    def draw(self, context):
        layout = self.layout
        layout.prop(self, 'autotint_offset')
        # layout.prop(context.scene, 'gp_autotint_offset')#, text = "offset"
    
    def invoke(self, context, event):
        self.autotint_offset = context.scene.gptoolprops.autotint_offset
        return self.execute(context)


def register():
    bpy.utils.register_class(GPT_OT_auto_tint_gp_layers)

def unregister():
    bpy.utils.unregister_class(GPT_OT_auto_tint_gp_layers)