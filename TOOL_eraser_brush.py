import bpy
from bpy.types import WorkSpaceTool
from gpu_extras.presets import draw_circle_2d
from time import time
from .utils import get_addon_prefs


class GPTB_WT_eraser(WorkSpaceTool):
    bl_space_type = 'VIEW_3D'
    bl_context_mode = 'PAINT_GPENCIL'

    # The prefix of the idname should be your add-on name.
    bl_idname = "gp.eraser_tool"
    bl_label = "Eraser"
    bl_description = (
        "This is a tooltip\n"
        "with multiple lines"
    )
    bl_icon = "brush.paint_vertex.draw"
    bl_widget = None
    bl_keymap = (
        ("gp.eraser", {"type": 'LEFTMOUSE', "value": 'PRESS'},
         {"properties": []}),
        ("wm.radial_control", {"type": 'F', "value": 'PRESS'},
         {"properties": [("data_path_primary", 'scene.gptoolprops.eraser_radius')]}),
    )
    
    bl_cursor = 'DOT'
    
    '''
    def draw_cursor(context, tool, xy):
        from gpu_extras.presets import draw_circle_2d

        radius = context.scene.gptoolprops.eraser_radius
        draw_circle_2d(xy, (0.75, 0.25, 0.35, 0.85), radius, 32)
    '''
        
    def draw_settings(context, layout, tool):
        layout.prop(context.scene.gptoolprops, "eraser_radius")

### --- REGISTER ---

## --- KEYMAP
addon_keymaps = []
def register_keymaps():
    addon = bpy.context.window_manager.keyconfigs.addon

    km = addon.keymaps.new(name="Grease Pencil Stroke Paint (Draw brush)", space_type="EMPTY", region_type='WINDOW')
    kmi = km.keymap_items.new("gp.eraser", type='LEFTMOUSE', value="PRESS", ctrl=True)

    prefs = get_addon_prefs()
    kmi.active = prefs.use_precise_eraser
    addon_keymaps.append((km, kmi))

def unregister_keymaps():
    for km, kmi in addon_keymaps:
        km.keymap_items.remove(kmi)
    addon_keymaps.clear()

def register():
    bpy.utils.register_tool(GPTB_WT_eraser, after={"builtin.cursor"})
    #bpy.context.window_manager.keyconfigs.default.keymaps['Grease Pencil Stroke Paint (Draw brush)'].keymap_items[3].idname = 'gp.eraser'

    register_keymaps()

def unregister():
    bpy.utils.unregister_tool(GPTB_WT_eraser)
    unregister_keymaps()
