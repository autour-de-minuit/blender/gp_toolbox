# Changelog

-- repository Archived on gitlab

3.0.2

- changed: Exposed `Copy/Move Keys To Layer` in Dopesheet(Gpencil), in right clic context menu and `Keys` menu.

3.0.1

- fixed: Crash after generating empty frames

3.0.0

- Update for Blender 4.0 (Breaking release, removed bgl to use gpu)
- fixed: openGL draw camera frame and passepartout

2.5.0

- added: Animation manager new button `Frame Select Step` (sort of a checker deselect, but in GP dopesheet)

2.4.0

- changed: Batch reproject consider camera movement and is almost 8x faster
- added: Batch reproject have "Current" mode (using current tool setting)

2.3.4

- fixed: bug when exporting json palettes containing empty material slots

2.3.3

- fixed: Bug with animation manager objects data

2.3.2

- fixed: Bug with animation manager when there is empty object in scene

2.3.1

- changed: Animation manager show hints when animation is enabled: fully, partially or not at all.

2.3.0

- added: Animation manager buttons are colored red when objects have disabled animation 
- fixed: Animation manager not enabling/disabling Action Groups
- fixed: Animation manager `List Disabled Anims` list groups as well

2.2.3

- fixed: Type error on realign ops

2.2.2

- fixed: draw_cam data not changed when working with multiple camera in a shot

2.2.1

- added: class View3D to calculate area 3d related coordinates

2.2.0

- added: _Remove redundant stroke_ in File checker (Just list duplicate numbers in "check only" mode)

2.1.6

- fixed: Prevent some keymaps to register when blender is launched in background mode

2.1.5

- fixed: false positive with checkfile in modifier layer target

2.1.4

- fixed: layer change msgbus not working at first activation of the addon

2.1.3

- fixed: decoralate Prefix and Suffix UI_lists scroll
- fixed: Problem when settings project namespaces
- added: Button to reset project namespace (to have the right order)

2.1.2

- added: `gp.brush_set` operator to manually assign a brush by name to a shortcut
  - preferably add shortcut to `Grease Pencil > Grease Pencil Stroke Paint Mode`

2.1.1

- added: follow curve show offset property in UI
- added: follow curve show clickable warning if object has non-zero location to reset location
- changed: created follow curve use `fixed offset`

2.1.1

- added: follow curve show offset property in UI
- added: follow curve show clickable warning if object has non-zero location to reset location
- changed: created follow curve use `fixed offset`

2.1.0

- added: 3 actions buttons:
  - create curve with follow path and go into curve edit
  - go back to object
  - got to curve edit (if follow path constraint exists with a curve target)
  - if follow path exists, button to remove constraint

2.0.11

- fix: prefix set by project environment

2.0.10

- fix: poll error in console

2.0.9

- fix: prefix/suffix UIlist actions trigger UI redraw to see changes live
- changed: PropertyGroups are now registered in their own file
- code: cleanup

2.0.8

- changed: suffix as UIlist in prefs,
- fix: prefix and suffix register and load

2.0.7

- fix: broken auto-fade with gp layer navigation when used with a customized shortcut.
- changed: supported version number to 3.0.0

2.0.6

- changed: Use prefixes toggle is enabled by default
- changed: prefixes are now set in preferences as a reorderable UI list with full name for description
  - This should not affect `PREFIXES` env variable (`'CO, LN'`)
  - Now can be also passed as prefix:name pair ex: `'CO : Color, LN : Line`

2.0.5

- changed: redo panel for GP layer picker has better name and display active layer name
- changed: some operator id_name to expose "add shortcut" in context menu for some button
- fix: error with draw_cam handler when no camera is active

2.0.3

- changed: `X-ray` to `In Front`, match original object property name

2.0.2

- added: possibility to select which point attribute is copied by GP layer copy to clipboard

2.0.1

- added: enable/disable camera animation separately in `animation manager`
  - for cam,disable properties like focal and shift (to be able to work on a fixed cam)
  - camera is not disable using `obj` anymore

2.0.0

- fix: error using playblast (api changes of Blender 3.0+)
- added: possibility to change playblast destination in preferences
- changed: playblast default is `playblast` folder instead of `images`
- changed: completely disable experimental precise eraser

1.9.9

- fix: Bug setting GP layers actions on/off
- fix: Add GP layers animation datas in `list disabled animation` report

1.9.8

- fix: animation manager `GP anim on/off` also toggle layers animation datas

1.9.7

- changed: `list modifiers visibility conflict` (used in `check file` or through search menu) now not limited to GP objects type.

1.9.6

- fix: icon removed in 3.0

1.9.5

- added: `Check file` also check if GP modifiers have broken layer target
- changed: `Check file` disable drawing guide is now On by default

1.9.4

- feat: renaming using the name_manager take care of changing layer target values in GP modifiers

1.9.3

- feat: Add an update button at bottom of preferences if addon is a cloned repository

1.9.2

- feat: Palette linker has a name exclusion list in preferences
  - Avoid linking some material that are prefered local to file
  - Default item in list : `line`

1.9.1

- fix: add error handling on palette linker when blend_path isn't valid anymore
- added: file checker entry, Disable all GP object `use lights` (True by default in addon pref `checklist`)
- added: WIP of Batch reproject all on cursor.
  - No UI. In search menu `Flat Reproject Selected On cursor` (idname `gp.batch_flat_reproject`)

1.9.0

- feat: New shortcuts:
  - `F2` in Paint and Edit to rename active layer
  - `Insert` add a new layer (same as Krita)
  - `Shift + Insert` add a new layer and immediately pop-up a rename box
  - `page up / page down` change active layer up/down with a temporary fade (settings in addon prefs)
- fix: error when tweaking `gp.duplicate_send_to_layer` shortcut

1.8.1

- fix: Gp clipboard paste `Paste layers` don't skip empty frames anymore

1.8.0

- feat: palette linker (beta), with pop-up from material stack dropdown
- feat: palette name fuzzy match
- code: add an open addon preference ops

1.7.8

- fix: reset rotation in draw cam mode keep view in the same place (counter camera rotation)

1.7.7

- feat: add copy path to `check link` ops with multiple path representation choices

1.7.6

- ui: expose (almost) all keymap added by the addon to allow user for customize/disable as needed
- ui: changed some names

1.7.5

- feat: Select/set by color and by prefix now works on every displayed dopesheet layer (and react correctly to filters)
- ui: exposed user prefs `Channel Group Color` prop in dopesheet > sidebar > View > Display panel
- add undo step for `W`'s select layer from closest stroke

1.7.4

- added: Pick layer from closest stroke in paint mode using quick press on `W` for stroke (and `alt+W` for fills)
- fix: copy-paste keymap error on background rendering

1.7.3

- added: show/hide gp modifiers if they are showed in render (in subpanel `Animation Manager`)

1.7.2

- added: `Object visibility conflict` in file check
  - print in console when object have render activated but not viewport (& vice-versa)
  - Standalone ops "List Object Visibility Conflicts" (`gp.list_object_visibility`)
- added: `GP Modifier visibility conflict` in file check.
  - print in console when gp modifiers have render activated but not viewport (& vice-versa)
  - Standalone ops "List GP Modifiers Visibility Conflicts" (`gp.list_modifier_visibility`)
- code: show_message_box utils can now receive operator in sublist (if 3 element)

1.7.1

- feat: Improved `Create Empty Frames` operator with mutliple filters to choose source layers

1.7.0

- removed: Obsolete operators and panels
  - Remove "line closer" panel as it's been a native tool for a while in 2.9x (stop register of `GP_guided_colorize > OP_line_closer`)
  - Remove "Render" subpanel, obsolete and not adapted to production
- ui: clean and refactor
  - Gp clipboard panel and aimation Manager subpanel layout to aligned columns (gain space)
  - add `GP` on each panel name for wuick eye search
  - follow cursor now in animation manager subpanel (should be in an extra menu or removed in the end)

1.6.9

- change: material picker (`S` and `Alt+S`) quick trigger, change is only triggered if key is pressed less than 200ms
  - this is made to let other operator use functionality on long press using `S`
- feat: material picker shortcut has now an enum choice to filter targeted stroke (fill, stroke, all)
  - by default `S` is still fill only
  - but `Alt+S` is now stroke only instead of all

1.6.8

- fix: reproject GP repeating projection on same frames
- fix: batch reproject all frame not poping dialog menu

1.6.7

- fix: error with operator `OP_key_duplicate_send` poll flooding console

1.6.6

- fix: inverted scene resolution from project environnement

1.6.5

- feat: check canvas alignement of the Gp object compare to chosen draw axis

1.6.4

- fix: disable multi-selection for layer naming manager
  - the dopesheet selection sometimes still consider layer as selected

1.6.3

<!-- - abort :: add checkbox to prevent from loading project environnement namespace -->
- add buttons to load manually environnement namespace

1.6.2

- disable keymap register for breakdowner on background

1.6.1

- removed: Auto Updater that was failing since 2.93
- prefs: add a checkbox to disable the "load base palette button" in UI.

1.6.0

- feat: Namespace upgrade
  - support pseudo group naming
  - add group and indent button
- feat: Fill tool shortcut for material color picker (from closest visible stroke)
  - `S` : get material closest *fill* stroke
  - `Àlt+S` : get closest stroke (fill or stroke)

1.5.8

- feat: Namespace improvement:
  - new suffixes list that generate suffix buttons
  - dynamic layer name field that show active (msgbus)
  - possible to disable the panel with an option

1.5.7

- feat: check list, specify in addon pref if you prefer a dry run (check without set)
- feat: check file can change the lock object mode state (do nothing by default)

1.5.6

- feat: layer drop-down menu include an operator for batch find/replace GP layers (idname: `gp.rename_gp_layers`)

1.5.5

- feat: check file: add check for filepath mapping (if all relative or all absolute)
- change: check file: disable resolution set by default

1.5.4

- feat: Layer manager
  - select/set layer prefix
  - select/set layer color
- code: refactor name builder function

1.5.3

- feat: layer aquick-prefix for layer using pref separator
  - list editable in addon pref
  - add interface button above layers
- code: added environnement variable for prefix and separator:
  - `PREFIXES` : list of prefix (comma separated uppercase letters between 1 and 6 character, ex: 'AN,SP,L')
  - `SEPARATOR` : Separator character to determine prefixes, default is '_' (should not be a special regex character)
- UI: add addon prefs namespace ui-box in project settings


1.5.2

- add environnement variables to set addon preferences project settings at register through `os.getenv('KEY')`:
  - `RENDER_WIDTH` : resolution x
  - `RENDER_HEIGHT` : resolution y
  - `FPS` : project frame rate
  - `PALETTES` : path to the blends (or json) containing materials palettes
  - `BRUSHES` : path to the blend containing brushes to load

1.5.1

- fix: eraser brush
- change: check file has custom check list in addon prefs

1.5.0

- feat: Eraser Brush Tool (Need to be enable in the preferences)

1.4.3

- feat: load brushes from blend
- ui: add load brushes within tool brush dropdown panel and in the top bar in drawmode
- pref: Set project brushes folder in addon preferences

1.4.2

- feat: new material cleaner in GP layer menu with 3 options
  - clean material duplication (with sub option to not clear if color settings are different)
  - fuse material slots that have the same materials
  - remove empty slots

1.4.1

- fix: custom passepartout size limit when dezooming in camera

1.4.0

- feat: Passepartout displayed in main cam (permanent draw handler)
- UI: add Straight stroke operator button in Toolbox if GP tools official addon is on
- UI: placed Toolbox Playblast in a subpanel
- UI: removed Onion skin and Autolock layer checkbox (not often used)
- UI: sent rarely `cursor follow` to bottom of the panel

1.3.3

- feat: show main cam frame when in draw_cam

1.3.2

- change: disable manip cam name drawing
- code: add initial support for main cam frame draw within camera view

1.3.1

- fix: native refresh error that rarely happen that doesn't completely refresh the scene on keyframe jump.  
  - Use a double frame change to ensure refresh.

1.3.0

- feat: new duplicate send to layer feaure - `ctrl + shift + D` in GP dopesheet

1.2.2

- fix: realign anim return error

1.2.1

- fix: Breakdowner initial error check

1.2.0

- feat: New depth move operator that handle both perspective and orthographic cam
- feat: Realign, added drawing plane checkbox to autoset to Front after realign
- UI: Reorganised realign panel
- UI: Switched part of the sidebar items to columns intead of basic layout to gain space
- doc: Added changelog file (moved list from readme)
- doc: relative link to changelog and FR_readme in main readme

1.1.0

- Important change : Remap relative is now disabled by default in addon preferences
- feat: Add realign operator in sidebar with reproject as true by default
- UI: Batch reproject all frames is now in menus. Same places as native reproject

1.0.9

- feat: Reproject all frames operator

1.0.8

- feat: Keyframe jump filter added in UI to change general behavior. Keymap own jump filter can override this new global settings if specified

1.0.7

- feat: Keyframe jump filter by type. User can now choose if the shortcut should jump on a specific keyframe type (All by default)

1.0.5

- GP copy-paste : Pasted stroke are now selected (allow to use it to quickly rip/split strokes with cut/paste on the same layer)

1.0.4

- UI: Better cam ref exposition in Toolbox panel
  - Access to opacity
  - merge activation bool with source type icon
- feat: Added a clear active frame operator (`gp.clear_active_frame` to add manually in keymaps)

1.0.3

- feat: add "Append Materials To Selected" to material submenu. Append materials to other selected GP objects if there aren't there.

1.0.2

- pref: Added option to disable always remap relative on save in addon-preference

1.0.1

- fix: copy paste problems
  - Get points uv_properties (used for brushed points)
  - Trigger an update on each pasted strokes, recalculate badly drawn uv and fills (works in 2.93+)

1.0.0

- Compatible with official grease pencil tools
  - removed box deform and rotate canvas that existed in other

0.9.3

- feat: keyframe jump keys are now auto-binded
- UI: added keyframe jump customisation in addon pref
- code: split keyframe jump in a separate file with his new key updater

0.9.2

- doc: Correct download link (important, bugged the addon install) + update
- code: added tracker url
- updater: remove updater temp file, reset minimum version, turn off verbose mode

0.9.1

- Public release
- prefs: added fps as part of project settings
  - check file use pref fps value (previously used harcoded 24fps value)
- cleanup: Remove wip GMIC-bridge tools that need to be done separately (if needed)
- update: apply changes in integrated copy-paste from the last version of standalone addon
- doc: Added fully-detailed french readme


0.8.0

- feat: Added background_rendering playblast, derivating from Tonton's playblaster
  - stripped associated properties from properties.py and passed as wm props.

0.7.2

- fix: Palette importer bug

0.7.0

- feat: auto create empty frame on color layer

0.6.3

- shortcut: added 1,2,3 to change sculpt mask mode (like native edit mode shortcut)

0.6.2

- feat: colorisation, Option to change stop lines length
- Change behavior of `cursor_snap` ops when a non-GP object is selected to mode: `surface project`
- Minor refactor for submodule register

0.6.1

- feat: render objects grouped, one anim render with all ticked object using manual output name 

0.6.0

- feat: Include GP clipoard's "In place" custom cut/copy/paste using OS clipboard

0.5.9

- feat: render exporter
  - Render a selection of GP object isolated from the rest
  - added exclusions names for GP object listing
  - setup settings and output according to a name
  - open render folder
- check file: set onion skin keyframe filter to 'All_type' on all GP datablock
- check file: set scene resolution to settings in prefs (default 2048x1080)

0.5.8

- feat: GP material append on active object from single blend file

0.5.7

- Added warning message for cursor snapping

0.5.6

- check file: added check for placement an projection mode for Gpencil.
- add a slider to change edit_lines_opacity globally for all GP data at once
- check file: auto-check additive drawing (to avoid empty frame with "only selected channel" in Dopesheet)

0.5.4

- feat: anim manager in his own GP_toolbox submenu:
  - button to list disabled anim (allow to quickly check state of the scene)
  - disable/enable all fcurve in for GP object or other object separately to paint
  - shift clic to target selection only
- check file: added disabled fcurved counter alert with detail in console

0.5.3

- fix: broken obj cam (add custom prop on objcam to track wich was main cam)
- check file option: change select active tool (choice added in addon preferences)

0.5.2

- Revert back obj_cam operator for following object (native lock view follow only translation)
- Changed method for canvas rotation to more robust rotate axis.
- Add operators on link checker to open containing folder/file of link
- Refactor: file checkers in their own file

0.5.1

- fix: error when empty material slot on GP object.
- fix: cursor snap on GP canvas when GP is parented
- change: Deleted obj cam (and related set view) operator
- change: blacker note background for playblast (stamp_background)
- feat: Always playblast from main camera (if in draw_cam)
- feat: Handler added to Remap relative on save (pre)
- ops: Check for broken links with porposition to find missing files
- ops: Added basic hardcoded file checker
  - Lock main cam
  - set scene percentage at 100
  - set show slider and sync range
  - set fps to 24

0.4.6

- feat: basic Palette manager with base material check and warning

0.4.5

- open blender config folder from addon preference
- fix: obj cam parent on selected object
- added wip rotate canvas axis file. still not ready to replace current canvas rotate:
  - freeview : bug when rotating free viewfrom cardianl views
  - camview: potential bug when cam is parented with some specific angle (could not reproduce)


0.4.4

- feat: added cursor follow handlers and UI toggle

0.4.3

- change playblast out to 'images' and add playblast as name prefix

0.4.2

- feat: GP canvas cursor snap wiht new `view3d.cusor_snap` operator
- fix: canvas rotate works with parented camera !
- wip: added an attmpt to replicate camera rotate modal with view matrix but no luck.

0.4.1

- feat: Alternative cameras: parent to main cam (roll without affecting main cam), parent to active object at current view (follow current Grease pencil object)

0.4.0

- Added a standalone working version of box_deform (stripped preferences keeping only best configuration with autoswap)

0.3.8

- UI: expose onion skin in interface
- UI: expose autolock in interface
- UI : putted tint layers in a submenu
- code: refactor, pushed most of class register in their owner file
- tool: tool to rename current or all grease pencil datablock with different name than container object

0.3.7

- UI: new interface with tabs for addon preferences
- UI: possible to disable color panel from preference (might be deleted if unusable)
- docs: change readme changelog format and correct doc

0.3.6

- UI: Stoplines : add a button for quickly set stoplines visibility.

0.3.5

- Fix : No more camera rotation undo when ctrl+Z on next stroke (canvas rotate push and undo)
- Fix: Enter key added to valid object-breakdown modal.

0.3.3

- version 1 beta (stable) of line gap closing tools for better bucket fill tool performance with UI

0.3.3

- version 1 beta of gmic colorize
- variant of `screen.gp_keyframe_jump` through keymap seetings

0.3.0

- new homemade [breakdowner operator for object](https://blenderartists.org/t/pose-mode-animation-tools-for-object-mode/1221322) mode with auto keymap : Shift + E
- GP cutter shortcut ops to map with `wm.temp_cutter` (with "Any" as press mode) or `wm.sticky_cutter` (Modal sticky-key version)

0.2.3

- add operator to `screen.gp_keyframe_jump`
- add shortcut to rotate canvas
- fix duplicate class

0.2.2

- separated props resolution_percentage parameter
- playblast options for launching folder and opening folder

0.2.1

- playblast feature
- Button to go zoom 100% or fit screen
- display scene resolution with res indicator
- Fix reference panel : works with video and display in a box layout.
- close pseudo-color panel by default (plan to move it to Gpencil tab)

0.2.0

- UI: Toggle camera background images from Toolbox panel
- UI: quick access to passepartout
- Feature: option to use namespace for pseudo color

0.1.5

- added CGC-auto-updater

0.1.3

- flip cam x
- inital stage of overlay toggle (need pref/multiple pref)

0.1.2

- subpanel of GP data (instead of direct append)
- initial commit with GP pseudo color