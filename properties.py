import bpy
from bpy.types import PropertyGroup
from bpy.props import (
                    IntProperty,
                    BoolProperty,
                    StringProperty,
                    FloatProperty,
                    EnumProperty,
                    )

from .OP_cursor_snap_canvas import cursor_follow_update
from .OP_layer_manager import layer_name_build

def change_edit_lines_opacity(self, context):
    for gp in bpy.data.grease_pencils:
        if not gp.is_annotation:
            gp.edit_line_color[3]=self.edit_lines_opacity


def update_layer_name(self, context):
    if not self.layer_name:
        # never replace by nothing (since there should be prefix/suffix)
        return
    if not context.object or context.object.type != 'GPENCIL':
        return
    if not context.object.data.layers.active:
        return
    layer_name_build(context.object.data.layers.active, desc=self.layer_name)
    # context.object.data.layers.active.info = self.layer_name
    

class GP_PG_FixSettings(PropertyGroup):

    check_only : BoolProperty(
        name="Dry run mode (Check only)",
        description="Do not change anything, just print the messages", 
        default=False, options={'HIDDEN'})

    lock_main_cam : BoolProperty(
        name="Lock Main Cam",
        description="Lock the main camera (works only if 'layout' is not in name)", 
        default=False, options={'HIDDEN'})
    
    set_scene_res : BoolProperty(
        name="Reset Scene Resolution",
        description="Set the scene resolution to current prefs project settings", 
        default=False, options={'HIDDEN'})

    set_res_percentage : BoolProperty(
        name="Reset Resolution Percentage To 100%",
        description="", 
        default=True, options={'HIDDEN'})

    set_fps : BoolProperty(
        name="Reset Fps",
        description="Set the framerate of the scene to current prefs project settings", 
        default=False, options={'HIDDEN'})

    set_slider_n_sync : BoolProperty(
        name="Dopesheets Show Slider And Sync Range",
        description="Toggle on the use of show slider and sync range", 
        default=True, options={'HIDDEN'})
    
    set_gp_use_lights_off : BoolProperty(
        name="Set Off Use lights On All Gpencil Objects",
        description="Uncheck Use lights on all grease pencil objects\nAt object level, not layer level (Object properties > Visibility > GP uselight)", 
        default=True, options={'HIDDEN'})

    check_front_axis : BoolProperty(
        name="Check If Draw Axis is Front (X-Z)",
        description="Alert if the current grease pencil draw axis is not front (X-Z)", 
        default=True, options={'HIDDEN'})

    check_placement : BoolProperty(
        name="Check Stroke Placement",
        description="Alert if the current grease pencil stroke placement is not Origin", 
        default=True, options={'HIDDEN'})

    set_pivot_median_point : BoolProperty(
        name="Set Pivot To Median Point",
        description="Change the pivot point to the most used median point", 
        default=True, options={'HIDDEN'})

    disable_guide : BoolProperty(
        name="Disable Drawing Guide",
        description="Disable constrained guide in draw mode", 
        default=True, options={'HIDDEN'})

    list_disabled_anim : BoolProperty(
        name="List Disabled Animation",
        description="Alert if there are disabled animations", 
        default=True, options={'HIDDEN'})
    
    list_obj_vis_conflict : BoolProperty(
        name="List Object Visibility Conflicts",
        description="Alert if some objects have different hide viewport and hide render values", 
        default=True, options={'HIDDEN'})

    list_gp_mod_vis_conflict : BoolProperty(
        name="List GP Object Modifiers Visibility Conflicts",
        description="Alert if some GP modifier have different show viewport and show render values", 
        default=True, options={'HIDDEN'})

    list_broken_mod_targets : BoolProperty(
        name="List GP Object modifiers broken target",
        description="Alert if some GP modifier have a target layer not existing in layer stack", 
        default=True, options={'HIDDEN'})

    autokey_add_n_replace : BoolProperty(
        name="Autokey Mode Add and Replace",
        description="Change autokey mode back to 'Add & Replace'", 
        default=True, options={'HIDDEN'})
    
    remove_redundant_strokes : BoolProperty(
        name="Remove Redundant Strokes",
        description="Remove GP strokes duplication. When points are exactly identical within the same frame", 
        default=True, options={'HIDDEN'})

    # set_cursor_type : BoolProperty(
    #     name="Set Select Cursor Mode",
    #     description="Set the type of the selection cursor (according to addon prefs)", 
    #     default=True, options={'HIDDEN'})

    ## default active tool to use
    select_active_tool : EnumProperty(
        name="Set Default Selection Tool", description="Active tool to set when launching check fix scene",
        default='builtin.select_lasso',
        items=(
            ('none', 'Dont change', 'Let the current active tool without change', 0),#'MOUSE_RMB'
            ('builtin.select', 'Select tweak', 'Use active select tweak active tool', 1),#'MOUSE_RMB'
            ('builtin.select_box', 'Select box', 'Use active select box active tool', 2),#'MOUSE_LMB'
            ('builtin.select_circle', 'Select circle', 'Use active select circle active tool', 3),#'MOUSE_MMB'
            ('builtin.select_lasso', 'Select lasso', 'Use active select lasso active tool', 4),#'MOUSE_MMB'
            ))

    ## check file path mapping type
    file_path_type : EnumProperty(
        name="Check File Path Mapping", description="Check if all path in scene are relative, absolute or disable the check",
        default='RELATIVE',
        items=(
            ('none', 'No Check', 'Do not check for path type', 0),
            ('RELATIVE', 'Relative', 'Check if all path are relative', 1),
            ('ABSOLUTE', 'Absolute', 'Check if all path are absolute', 2),
            ))

    ## check set lock object mode
    lock_object_mode : EnumProperty(
        name="Set Lock Object Mode", description="Set 'Lock object mode' parameter check'",
        default='none',
        items=(
            ('none', 'Do nothing', 'Do not set', 0),
            ('LOCK', 'Lock object mode', 'Toggle lock object mode On', 1),
            ('UNLOCK', 'Unlock object mode', 'Toggle lock object mode Off', 2),
            ))

class GP_PG_ToolsSettings(PropertyGroup):
    eraser_radius : IntProperty(
        name="Eraser Radius", description="Radius of eraser brush", 
        default=20, min=0, max=500, subtype='PIXEL')

    drawcam_passepartout : BoolProperty(
        name="Show cam passepartout",
        description="Show a darkened overlay outside image area in Camera view", 
        default=True,
        options={'HIDDEN'})

    autotint_offset : IntProperty(
        name="Tint hue offset", description="offset the tint by this value for better color", 
        default=0, min=-5000, max=5000, soft_min=-999, soft_max=999, step=1,
        options={'HIDDEN'})#, subtype='PERCENTAGE'

    autotint_namespace : BoolProperty(
        name="Use prefix", description="Put same color on layers unsing the same prefix (separated by '_') of full name withjout separator", 
        default=True,
        options={'HIDDEN'})

    resolution_percentage: IntProperty(
        name="Resolution %", description="Overrides resolution percentage for playblast", 
        default = 50, min=1, max= 100, subtype='PERCENTAGE')#, precision=0
    
    cursor_follow : BoolProperty(
        name='Cursor Follow', description="3D cursor follow active object animation when activated",
        default=False, update=cursor_follow_update)

    edit_lines_opacity : FloatProperty(
        name="Edit Lines Opacity", description="Change edit lines opacity for all grease pencils", 
        default=0.5, min=0.0, max=1.0, step=3, precision=2, update=change_edit_lines_opacity)

    ## render
    name_for_current_render : StringProperty(
        name="Render_name", description="Name use for render current",
        default="")

    keyframe_type : EnumProperty(
        name="Keyframe Filter", description="Only jump to defined keyframe type", 
        default='ALL', options={'HIDDEN', 'SKIP_SAVE'},
        items=(
            ('ALL', 'All', '', 0), # 'KEYFRAME'
            ('KEYFRAME', 'Keyframe', '', 'KEYTYPE_KEYFRAME_VEC', 1),
            ('BREAKDOWN', 'Breakdown', '', 'KEYTYPE_BREAKDOWN_VEC', 2),
            ('MOVING_HOLD', 'Moving Hold', '', 'KEYTYPE_MOVING_HOLD_VEC', 3),
            ('EXTREME', 'Extreme', '', 'KEYTYPE_EXTREME_VEC', 4),
            ('JITTER', 'Jitter', '', 'KEYTYPE_JITTER_VEC', 5),
            ))

    layer_name : StringProperty(
        name="Layer Name",
        description="The layer name, should describe the content of the layer",
        default="",
        update=update_layer_name)# update=None, get=None, set=None

    """ 
    reconnect_parent = bpy.props.PointerProperty(type =bpy.types.Object,poll=poll_armature)
    render_settings = bpy.props.BoolProperty(default = False)
    render_color = bpy.props.BoolProperty(default = True)
    render_contour = bpy.props.BoolProperty(default = False)
    precision = bpy.props.IntProperty(default = 75,subtype = 'PERCENTAGE',min=0,max=100)

    border_render = bpy.props.BoolProperty(default = False)

    spacialize = bpy.props.BoolProperty(default = False)
    depth = bpy.props.FloatProperty(default = 2.0)

    extra_tools = bpy.props.BoolProperty(default = False)
    enable_ob_filter = bpy.props.BoolProperty(default = False)
    auto_cursor = bpy.props.BoolProperty(default = True)

    opacity_layers = bpy.props.FloatProperty(min=0,max=1,default = 1,update = update_layers_opacity)

    stroke_select =  bpy.props.EnumProperty(items = [("POINT","Point",""),("STROKE","Stroke","")],update = update_selection_mode)
    """

class GP_PG_namespace_props(PropertyGroup):

    tag : StringProperty(
        name="Tag", description="Layer namespace tag (prefix/suffix)",
        default="")
    name : StringProperty(
        name="Name", description="Name that represent this prefix (used as hint and tooltip)",
        default="")
    hide : BoolProperty(
        name="Hide", description="Hide this prefix from layer prefix management",
        default=False)
    
    is_project : BoolProperty(
        name="Project", description="Show this propery was set by project environnement (not deletable if that's the case)",
        default=False)

class GP_PG_namespaces(PropertyGroup):
    idx : IntProperty(default=-1)
    namespaces : bpy.props.CollectionProperty(type=GP_PG_namespace_props)

classes = (
    # Prefix/suiffix prefs prop group
    GP_PG_namespace_props,
    GP_PG_namespaces,

    ## General toolbox settings
    GP_PG_FixSettings,
    GP_PG_ToolsSettings,
)

def register():
    for cls in classes:
        bpy.utils.register_class(cls)

def unregister():
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)