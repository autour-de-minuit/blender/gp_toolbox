# GP toolbox

Blender addon - Boîte à outils de grease pencil pour la production d'animation.

## /!\ Ce dépôt est une archivé !

Le project mis à jour est accessible sur le gitea d'ADM: https://git.autourdeminuit.com/autour_de_minuit/gp_toolbox


---
Tout ce qui apparaît ci-dessous est lié à au dépôt git archivé

**[Télécharger la dernière version](https://gitlab.com/autour-de-minuit/blender/gp_toolbox/-/archive/master/gp_toolbox-master.zip)**

**[Demo video](https://www.youtube.com/watch?v=Htgao_uPWNs)**

**[English Readme Doc](README.md)**

Il est recommandé d'activer l'addon natif _Grease pencil tools_ en parallèle qui ajoute des outils essentiels:  
Rotation du Canvas, Boîte de déformation, Scrub dans la timeline, Navigation de calque facilité...

## Fonctionnalités et détails

### Variables d'environnement

> Cette partie s'adresse surtout aux dévellopeurs qui préparent les environement de projets.

Depuis la version 1.5.2, les variables d'environnement suivantes peuvent surcharger les préférences projet dans blender (au moment du register)

`RENDER_WIDTH` : résolution x  
`RENDER_HEIGHT` : résolution y  
`FPS` : Vitesse du projet  
`PALETTES` : Chemin vers le dossier des blends (ou json) contenant des palettes de matériaux  
`BRUSHES` : Chemin vers le dossier des blends contenant les brushes a charger  
`PREFIXES` : Liste de prefixes du projet pour les noms de calques  
`SUFFIXES` : Liste de suffixes (2 caractères majuscule, séparé par des virgule. ex: 'OL,UL')   
`SEPARATOR` : Caractère de séparation pour determiner les prefixes, par défaut '_' (ne doit pas être un caractère spécial de regex !)  

### Exposition dans l'UI de fonction native

Exposition de certains attribut déjà existant utiles mais trop "loins" dans l'interface

Expose les options suivantes:

- Zoom 1:1 - Vue en cam prend le zoom 100% en fonction de la résolution (ops `view3d.zoom_camera_1_to_1`)
- Zoom fit - Ajuste la vue pour que la cam soit vue en totalité dans l'écran (ops `view3d.view_center_camera`)
- Onion skin - coche des overlays
- autolock layer - coche du sous-menu de l'UI list des layers
- In Front - met en avant l'option `In Front` des propriété de l'objet
- passepartout de caméra - active-désactive + opactité
- liste de boutons pour activer/désactiver les background de caméra (tous ou individuellement) avec icone par type.

**Edit line opacity** - Il est pratique de pouvoir cacher l'edit line pour avoir un meilleur aperçu du rendu lors du sculpt par exemple. C'est une option lié au layer. Ce slider appelle une fonction pour affecter tout les layers de tout les objets au lieu de se cantonner au layer courant.

### Action passive

- Ajoute un handler qui déclenche un remmapage des chemins en relatif à chaque sauvegarde.
### Tools principaux d'anim:


**Système de Main cam VS draw cam / action cam** - pour permettre le roll du viewport depuis la vue caméra (sans affecter la caméra def) la draw cam peut être activée. c'est simplement une duplication de la caméra principale (main) qui partage la même data, et se retrouve parenté à cette dernière. On peut ainsi se permettre de faire le roll via rotate canvas. Solution de fortune tant que blender ne permet pas de roll la vue en cam.  

L'action cam (peut-être renommé en follow_cam ?) fonctionne sur le même principe que la draw sauf qu'elle se parente à l'objet selectionné. Le but est de pouvoir suivre un objet en mouvement pour le dessiner en continu sans nécessairement désactiver les anims. Concrètement, désactiver les animation d'objets est sans doute plus clean et sans doute moins galère donc option a potentiellement retirer dans le futur.

**GP keyframe jump** (auto bind et personnalisable dans les addon prefs depuis 0.9.3) - Essentiel ! Permet d'ajouter une ou plusieurs paire de raccourcis pour aller de keyframe en keyframe (filtrage du saut personnalisable). Lance l'operateur `screen.gp_keyframe_jump`, (si ajout manuel, rajouter une seconde keymap avec la propriété `next` décoché pour faire un saut arrière)

**Breakdown en mode objet** (`Shift+E`) - Breakdown en pourcentage entre les deux keyframes (pose une clé si l’auto-key est actif et utilise le keying set si actif). Même comportement et raccourci que le breakdown de bones en pose mode (juste qu'il n'existait bizzarement pas en objet)

**Snap cursor** - Snap le curseur sur le canvas de GP (respecte les paramètres "tool setting" qui définit le comportement du canvas)  
À placer dans la keymap manuellement pour le moment en utilisant l'id `view3d.cusor_snap` (original `view3d.cursor3d`)

**Playblast** - Créer un playblast de l'animation dans le dosssier du blend.  
Depuis la version 0.8.0 le playblast a été remplacé par le bg_playblast (adaptation de [playblaster](https://github.com/samytichadou/Playblaster) de Samy Tichadou)

**GP clipboard** (existe dans son propre panel) - Couper-copier-coller les clés en place (world space). Auto-keymap sur `Ctrl + Shift + X/C/V`
Stocke les traits dans le presse paier de l'OS (permet la copie facile entre blends)
Le "couper" est également plus naturel (conserve les points d'extrémité sur les strokes restants).  
Permet également de copier l'intégralité des layers selectionnés avec le bouton dédié (pas de raccourcis).

**check files** - série de check écris en dur dans le code. Pratique pour "fixer" rapidement sa scène:
la liste est visible et modifiable dans l'onglet "Check list" des preférences d'addons.
`Ctrl + Clic` sur le bonton permet de lister les changement sans les appliquer
Voilà quelques exemples:
    - Lock main cam
    - set scene res to def project res (specified in addon prefs)
    - set scene percentage at 100:
    - set show slider and sync range in opened dopesheet
    - set fps to 24 (need generalisation with addonpref choice)
    - set select cursor type (according to prefs ?)
    - GP use additive drawing (else creating a frame in dopesheet makes it blank...)
    - GP stroke placement/projection check (just warn if your not in 'Front')
    - Warn if there are some disabled animation (and list datapath)
    - Set onion skin filter to 'All type'

EDIT: _rotate canvas_ et _box deform_ ont été retiré dans la version 1.0 car déja intégré à l'addon natif **grease pencil tools** depuis la 2.91 (activez simplement cet addon)  
> **Rotate canvas** (`ctrl + alt + clic-droit`) - Différence avec celui intégré a grease pencil tools : la rotation en vue cam n'est possible que si on est dans une caméra de manipulation (`manip_cam`) pour éviter de casser l'anim le roll de la cam principale.

> **Box deform** (`Ctrl+T`) - Déformation 4 coins (Déjà dans _Grease pencil tools_ donc a potentiellement retirer pour éviter de possible confits/redondances, mais d'un autre côté la version intégrée peut être customisée et corriger d'éventuel souci sans attendre les updates de la version native...)

### moins important

**Mirror flip X** - Applique simplement un `scale.x -1` sur la cam active (un petit indicateur s'affiche pour indiquer qu'on est en mirror, re-cliquer sur le bouton permet de rétablir le scale d'origine)


**Cursor Follow** (Utilise un handler) - une fois activé, permet de faire suivre le curseur 3D sur un objet animé.  
Souci connu: Il y a un décalage d'une frame une fois activé sur un nouvel objet, il faut le recaler une fois.

**Animation manager** - Permet d'activer/désactiver les anims d'objets de tout les objets (ou uniquement les objets de type Grease pencil)

### Présent mais pas utilisés

**Sticky key tool** - Les raccourci vers des tools temporaires sont très pratique, on peut facilement créer un raccourci qui appelle le tool durant la presion d'un bouton. Cet operator est un modal qui permet de faire la même chose mais en activant simplement le tool si l'appui sur touche est rapide.  
À placer manuellement si besoin : idname : `wm.sticky_cutter` (fichier: `OP_temp_cutter.py > GPTB_OT_sticky_cutter` )

### À potentiellement mettre de côté (peu utilisé)

**Tint layer** - Permet de mettre une teinte aléatoire par calque afin de les différencier rapidement. Souci de cet opérateurm, il ne faut pas l'utiliser si la paropriétée `tints` des layers est utile aux projet car il modifie cette couleur.

<!-- **Colorize** (gros WIP) - un sous ensemble d'outils qui était censé permettre de faire du remplissage bitmap via des color spots en envoyant deux set d'images rendu automatiquement à GMIC en ligne de commande et recalé la séquence de résultat en BG de Cam. Finalement abandonné, pas eu le temps de finir la mise au point (malgré des résultats préliminaires intéressant).
Mais trop long a mettre en place, trop hackeu, et surtout c'est dommage de basculer sur du bitmap, la source de couleur doit rester au maximum GP/vecto. -->

## colorisation

**Create empty frame** - Permet de créer des frames vides sur un calques en se basant sur les frames des calques choisis (permet de faire un key to key sur le calque actif ensuite sur les key pré-créée pour faire sa colo).  
Sinon pratique pour la colo, on peut aussi ajouter un autre `screen.gp_keyframe_jump` operator en activant le filtre (all layers) dans les options  


## tools supplémentaires

**Check links** (pop une fenêtre) - Permet de lister les liens de la scène, voir si il y en a des cassés et ouvrir le dossier d'un lien existant et  copier un lien.

<!-- **Auto update** - Un système de mise à jour est diponible dans les addon prefs, autocheck possible (désactivé par défaut). Utilise [CGcookie addon updater](https://github.com/CGCookie/blender-addon-updater)) -->

## raccourcis supplémentaires

Viewport:

- `W` (stroke) et `Alt + W` (fill) Sélectionne un calque d'après le trait le plus proche du curseur (en paint mode)

- `S` (fill) et `Alt + S` (stroke) selectionne un matériaux d'après le trait le plus proche du curseur. (en paint fill mode) (la touche doit pressé/relaché en moins de 200ms)

- `F2` Pop-up pour renommer le calque actif (en paint et en edit mode)

- `Insert` Ajoute un nouveau layer (comme dans Krita)

- `Shift + Insert` Ajoute un nouveau layer et immédiatement apelle le pop-up pour renommer le calque

- `page up / page down` change le calque actif en estompant temporairement les autres calques (la force d'estompe est personalisable dans les prefs de l'addon)

- `Shift + E` breakdown sur l'animation d'objet en mode Objet (Fonctionne comme celui disponible sur des bones en pose mode).

- `Ctrl + Shift + X/C/V` - Couper/Copier/Coller en World Space (indépendamment de la position de l'objet)

Dopesheet:

- `Ctrl + Shift + X` Couper une clé et l'envoyer sur un autre calque

- `Ctrl + Shift + D` Dupliquer une clé et l'envoyer sur un autre calque

Sculpt mode:

- `1`, `2`, `3` (rangée au-dessus des lettres) Bascule les filtres de selection par Points/Strokes, comme en edit mode.

<!-- Grease pencil 3D cursor: Surcharge du raccourci curseur 3D pour le snapper à la surface du grease pencil. (Raccourci à remplacer manuellement dans la keymap, idname:`view3d.cusor_snap`, idname de l'opérateur de curseur natif `view3d.cursor3d`. Pas forcément utile si il n'y a pas de mix de 2D/3D.)
Le mieux reste d'avoir un raccourci dédié, séparé de celui d'origine... -->

---

### Idées:

- Permettre de rendre avec la résolution spécifié dans le nom de la caméra active
(utile dans les projet rendu a la résolution du BG mais ou la résolution finale peut être utilisé pour un bout-a-bout)

- Faire un import-export des réglage généraux en json (Déjà une bonne partie du code dans Pipe sync)
pour set : Résolution du film, dossier palette, render settings...

- opt: exposer les "tool setting" de placement de canvas en permanence dans la sidebar (visible seulement en draw)

- Déplacer automatiquement la vue "Face" au GP (en fonction des Gpencil view settings)

- Déplacer les clés de dopesheet en même temps que les clés de GP (Déjà Créer par Tom Viguier sur les repos d'[Andarta](https://gitlab.com/andarta-pictures)

---

[Liste des changements ici](CHANGELOG.md)